# Kopano Contacts Exporter

### Requirements

The following python3 modules need to be installed either from pip or with distribution packages:

- io
- csv
- configparser
- logging
- flask
- json

These dependencies can be fulfilled on Debian 9 by issuing 

```bash
# apt install python3-kopano python3-flask
```

### Configuration

kopano-contacts-exporter uses a configured token (`server_token` in `kopano-contacts-exporter.cfg`) to authenticate incoming requests.

This value should be set to an unique value. When accessed over the network connections should be secured by putting a reverse proxy infront of it.
Such a reverse proxy can be accomplished in Apache, by adding `ProxyPass /contacts/ http://localhost:7070/contacts/` to the vhost for Kopano WebApp.


### Start manually

```bash
# cp kopano-contacts-exporter.cfg /etc/kopano
# python3 kopano-contacts-exporter.py
```

### Start with provided systemd service file

```bash
# cp kopano-contacts-exporter.cfg /etc/kopano/
# cp kopano-contacts-exporter.service /etc/systemd/system
# cp kopano-contacts-exporter.py /usr/local/sbin
# chmod +x /usr/local/sbin/kopano-contacts-exporter.py
# systemctl enable kopano-contacts-exporter
# systemctl start kopano-contacts-exporter
```

Verify if it is working

```bash
# journalctl -f -u kopano-contacts
```

### Example Csv dump

```bash
# curl "localhost:7070/contacts/csv/?token=6279645b6f646769784b617a36202862&user=kopano"
```

### Example JSON dump

```bash
# curl "localhost:7070/contacts/json/?token=6279645b6f646769784b617a36202862&user=kopano"
```


### Potential Todo's

- Return Contacts from other (configured) folders.
- Run the service with uwsgi/gunicorn.
