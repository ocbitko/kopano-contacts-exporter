#!/usr/bin/python3
import configparser
import csv
import io
import logging

import kopano
from flask import Flask, request, Response, jsonify

description = {
    'en':
    ('Displayname', 'First name', 'Last name', 'Job title', 'Company', 'Department', 'Room',
     'Street', 'Postalcode', 'City', 'State', 'Country',
     'Street private', 'Postalcode private', 'City private', 'State private', 'Country private',
     'E-Mail', 'E-Mail 2', 'E-Mail 3', 'Website',
     'Tel. Business', 'Tel. Business 2', 'Tel. Private', 'Tel. Private 2', 'Tel. Mobile'),
    'de':
    ('Angezeigter Name', 'Vorname', 'Nachname', 'Position', 'Unternehmen', 'Abteilung', 'Raum',
     'Straße', 'PLZ', 'Ort', 'Bundesland', 'Land',
     'Straße privat', 'PLZ privat', 'Ort privat', 'Bundesland privat', 'Land privat',
     'E-Mail', 'E-Mail 2', 'E-Mail 3', 'Webseite', 'Tel. Geschäftlich', 'Tel. Geschäftlich 2',
     'Tel. Privat', 'Tel. Privat 2', 'Tel. Mobil')
}


def getconfig():
    config = configparser.ConfigParser()
    try:
        config.read('/etc/kopano/kopano-contacts-exporter.cfg')
        server_bind = config.get('server', 'server_bind')
        server_port = config.getint('server', 'server_port')
        server_token = config.get('server', 'server_token')
        response = config.getboolean('response', 'download')
        language = config.get('response', 'language')
        log_level = config.get('logging', 'log_level')
        log_file = config.get('logging', 'log_file')
        server_socket = config.get('kopano', 'server_socket', fallback=None)
        sslkey_file = config.get('kopano', 'sslkey_file', fallback=None)
        sslkey_pass = config.get('kopano', 'sslkey_pass', fallback=None)
        auth_user = config.get('kopano', 'auth_user', fallback=None)
        auth_pass = config.get('kopano', 'auth_pass', fallback=None)
        options = {
            'server_socket': server_socket,
            'sslkey_file': sslkey_file,
            'sslkey_pass': sslkey_pass,
            'auth_user': auth_user,
            'auth_pass': auth_pass
        }
        return options, server_bind, server_port, server_token, response, language, log_level, log_file
    except Exception as e:
        exit('Configuration {}\n please check kopano-contacts-exporter.cfg'.
             format(e))


def contacts2csv(options, user, language):

    output = io.StringIO()
    writer = csv.writer(
        output,
        dialect='excel',
        delimiter=';',
        quoting=csv.QUOTE_ALL,
        lineterminator='\r\n')
    writer.writerow(description[language])
    contacts = user.store.contacts
    contacts_folders = [contacts]
    if contacts.subfolder_count > 0:
        contacts_folders = contacts_folders + list(contacts.folders())
    for folder in contacts_folders:
        for contact in folder.items():
            if len(contact.business_phones) > 0:
                business0 = contact.business_phones[0]
            else:
                business0 = ''

            if len(contact.business_phones) == 2:
                business1 = contact.business_phones[1]
            else:
                business1 = ''

            if len(contact.home_phones) > 0:
                homes0 = contact.home_phones[0]
            else:
                homes0 = ''

            if len(contact.home_phones) == 2:
                homes1 = contact.home_phones[1]
            else:
                homes1 = ''

            writer.writerow(
                (contact.normalized_subject, contact.first_name, contact.last_name, contact.job_title,
                 contact.company_name, contact.department, contact.office_location,
                 contact.business_address.street, contact.business_address.postal_code, contact.business_address.city,
                 contact.business_address.state, contact.business_address.country,
                 contact.home_address.street, contact.home_address.postal_code, contact.home_address.city,
                 contact.home_address.state, contact.home_address.country,
                 contact.email1, getattr(contact, 'email2', ''), getattr(contact, 'email3', ''),
                 contact.business_homepage, business0, business1, homes0, homes1, contact.mobile_phone))
    return output.getvalue()


def contacts2json(options, user):

    output = []

    for contact in user.store.contacts.items():

        if len(contact.business_phones) > 0:
            business0 = contact.business_phones[0]
        else:
            business0 = ''

        if len(contact.business_phones) == 2:
            business1 = contact.business_phones[1]
        else:
            business1 = ''

        if len(contact.home_phones) > 0:
            homes0 = contact.home_phones[0]
        else:
            homes0 = ''

        if len(contact.home_phones) == 2:
            homes1 = contact.home_phones[1]
        else:
            homes1 = ''

        output.append({
            'Primary key': contact.guid,
            'Displayname': contact.normalized_subject,
            'First name': contact.first_name,
            'Last name': contact.last_name,
            'Position': contact.job_title,
            'Company': contact.company_name,
            'Department': contact.department,
            'Room': contact.office_location,
            'Street': contact.business_address.street,
            'Postalcode': contact.business_address.postal_code,
            'City': contact.business_address.city,
            'State': contact.business_address.state,
            'Country': contact.business_address.country,
            'Street private': contact.home_address.street,
            'Postalcode private': contact.home_address.postal_code,
            'City private': contact.home_address.city,
            'State private': contact.home_address.state,
            'Country private': contact.home_address.country,
            'Road other': contact.other_address.street,
            'Postalcode other': contact.other_address.postal_code,
            'Town other': contact.other_address.city,
            'State other': contact.other_address.state,
            'Country other': contact.other_address.country,
            'E-Mail': contact.email1,
            'E-Mail 2': getattr(contact, 'email2', ''),
            'E-Mail 3': getattr(contact, 'email3', ''),
            'Website': contact.business_homepage,
            'Tel. Business': business0,
            'Tel. Business 2': business1,
            'Tel. Private': homes0,
            'Tel. Private 2': homes1,
            'Tel. Mobile': contact.mobile_phone
        })

    return output


def main():
    (options, server_bind, server_port, server_token, response, language,
     log_level, log_file) = getconfig()

    log_level = getattr(logging, log_level.upper(), 'INFO')
    logging.basicConfig(
        format='%(levelname)s %(message)s', filename=log_file, level=log_level)

    app = Flask('Kopano Contacts Exporter')
    logging.info('Kopano Contacts Exporter Service started')
    app.config['JSON_AS_ASCII'] = False

    server = kopano.Server(server_socket=options['server_socket'],
                         sslkey_file=options['sslkey_file'],
                         sslkey_pass=options['sslkey_pass'],
                         auth_user=options['auth_user'],
                         auth_pass=options['auth_pass'])

    @app.route('/contacts/csv/', methods=['GET'])
    def publish_csv():

        token = request.args.get('token', False)
        user = request.args.get('user', False)

        if token == server_token and user:
            try:
                result = contacts2csv(options, server.user(user), language)
                logging.info('contacts csv send for user {}'.format(user))
                if response:
                    return Response(
                        result,
                        mimetype="text/csv",
                        headers={
                            "Content-disposition":
                            "attachment; filename=contacts.csv"
                        })
                else:
                    return result

            except Exception as e:
                logging.error('contacts2csv: {}'.format(e))
                return ""
        else:
            logging.warning('No or invalid server_token provided')
            return ""

    @app.route('/contacts/json/', methods=['GET'])
    def publish_json():

        token = request.args.get('token', False)
        user = request.args.get('user', False)

        if token == server_token and user:
            try:
                result = contacts2json(options, server.user(user))
                logging.info('contacts json send for user {}'.format(user))
                return jsonify(result)

            except Exception as e:
                logging.error('contacts2json: {}'.format(e))
                return jsonify([])

        else:
            logging.warning('No or invalid server_token provided')
            return ""

    app.run(host=server_bind, port=server_port, debug=False)


if __name__ == '__main__':
    main()
