# Integrating Kopano Addressbooks into estos MetaDirectory


## What is MetaDirectory?

[MetaDirectory](https://www.estos.com/products/metadirectory) is a service running on a Microsoft Windows host. The website describes it as:

> The MetaDirectory is a LDAP server for quick access to contact data. It merges different databases into a single, consistent LDAP directory that can be used across the company. As a result, relevant data, such as telephone numbers and other contact details, are available to all employees. The intelligent data processing allows for easy searching and the fast presentation of results - even with large databases.

Through this it can be connected to telephony systems to present the user with caller information on incoming calls as well as initiate outgoing calls. 

## How does it relate to Kopano?

Customers using the Kopano WebApp or DeskApp would like to have an integration of their contact data to have them displayed as well.

## Technical information

MetaDirectory allows import of contact data through cvs files, which could be generated with a pre- and cleaned up with a post-command.
This has been identified as a good way to get started quickly. On the long term the Kopano RestAPI could be a good alternative approach.

General downside is that for each user an own data import has to be configured (same would be with the Rest approach, or any other application that should provide data only to a specific user). In the future estos wants to provide a way to have a sort of templated data import to mass configure this.

### Steps

Since MetaDirectory is running on windows, we cannot directly work with python-kopano. Instead we need a two part approach:

1. a webservice on the Kopano host that sends out the contact data
2. a small windows component that requests the contact data and creates the csv data

The webservice should have a minimal configuration file to configure the connection to Kopano (Socket, username/password or SSL client certificate) and have a pre-defined secret the windows component can use to authenticate itself.
It the webservice should be secured, then this shall be done through a reverse proxy.

### Webservice description

The webservice could be called like the following:

`http://kopano-server/kopano-csv?token=pre-configured-secret&user=username`

Where `pre-configured-secret` is the value from the configuration file and `username` is the username of the Kopano user the addressbook should be returned for.

The response could either directly be the csv file or any other response date, which is easy to process on the windows side.